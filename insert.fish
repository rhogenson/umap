#!/usr/bin/env fish
set script_name (basename (status -f))
set map_path (dirname (realpath (status -f)))/map

function usage
	echo Before running $script_name, have the intended unicode character in the clipboard.
	echo When prompted, enter the CLDR short name of the character.
	echo Entries will be written to $map_path.
	exit
end

argparse -n $script_name -X 0 h/help -- $argv; or usage
if set -q _flag_h
	usage
end

set p (dmenu -p 'Enter char name')
if test -n "$p"
	echo -- $p\t(xclip -o -sel clip) >>$map_path
	sort -u $map_path -o $map_path
end
