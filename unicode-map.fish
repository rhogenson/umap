#!/usr/bin/env fish
set script_name (basename (status -f))
set map_path (dirname (realpath (status -f)))/map

function usage
	echo $script_name will read entries from $map_path.
	echo Use the insert script to add new entries.
	exit
end

argparse -n (basename (status -f)) -X 0 h/help -- $argv; or usage
if set -q _flag_h
	usage
end

set f (dirname (realpath (status -f)))/map
sed -n s/^(sed -n 's/\t.*$//p' $f | dmenu)\t//p $f | xclip -sel clip
